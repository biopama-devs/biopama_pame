(function ($, Drupal) {
  Drupal.behaviors.thumbnailHover = {
    attach: function (context, settings) {
      $('.zoom-on-hover', context).once('thumbnailHover').each(function () {
        var $this = $(this);
        var fullSizeUrl = $(this).data('original-url');

        $this.hover(
          function () {
            $("#modal_image").attr('src',fullSizeUrl);
            $('#modal_image_zoom').modal('show');
          },
          function () {
            $('#modal_image_zoom').modal('hide');
          }
        );

      });
    }
  };
})(jQuery, Drupal);
