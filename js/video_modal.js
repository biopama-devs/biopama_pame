(function ($, Drupal) {
  'use strict';


  function unescapeHTML(str) {
    return $('<textarea/>').html(str).text();
  }



  Drupal.behaviors.videoModal = {
    attach: function (context, settings) {
      $('.video-modal', context).once('video-modal').each(function () {
        const $modal = $(this);
        const modalId = $modal.attr('id');
        const entityId = modalId.replace('modal_', '');
        const videoContent = unescapeHTML($modal.attr('data-video-content'));

        $modal.on('show.bs.modal', function () {
          $(`#video-placeholder-${entityId}`).html(videoContent);
        });

        $modal.on('hide.bs.modal', function () {
          $(`#video-placeholder-${entityId}`).empty();
        });
      });
    }
  };

  // Add Ajax event listener
  $(document).ajaxComplete(function (event, xhr, settings) {
    console.log('ajaxComplete');
    Drupal.attachBehaviors(document, drupalSettings);
  });

})(jQuery, Drupal);
