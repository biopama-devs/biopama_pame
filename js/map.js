var selSettings = {
    paName: 'default',
	WDPAID: 0,
    countryName: 'trans-ACP',
    regionID: null,
	regionName: null,
	ISO2: null,
	ISO3: null,
	NUM: null,
};
var sel_filters = {
	'wdpaid': null,
	'region': null,
	'iso3':null,
	'pame_year':null,
	'pame_methodology':null,
	'pa_type':null
};

//var firstRun = true;

var dt_table;

var biopmaApiBaseUrl = "https://api.biopama.org/api/pame/function";

jQuery(document).ready(function($) {

	function createDTables(){
		var $table = $('.biopama_tables table');

		dt_table = $.fn.createDataTable(
			$table,
			{
				columns: [
					{ title: "WDPAID", data : "wdpaid" },
					{ title: "Region", data : "region" },
					{ title: "ISO-3 Code", data : "iso3" },
					{ title: "Country", data : "country_name" },
					{ title: "Type", data : "pa_type" },
					{ title: "Protected Area", data : "name" },
					{ title: "Designation", data : "pame_design" },
					{ title: "Methodology", data : "pame_methodology" },
					{ title: "Year", data : "pame_year" },
				],
				dom: 'Bfrtp',
				pageLength : 8,
				attribution: 'Data Source: https://pame.protectedplanet.net/ | Global Database on Protected Area Management Effectiveness (GD-PAME)',
				createdRow: function(row, data, dataIndex){
					$(row).on('click',function(){
						selSettings.WDPAID = parseInt($(this).find('td:first-child').text());
						$().mapZoomToPA(mymap, selSettings.WDPAID);
						$('#table_assessments tbody tr').removeClass('selected');
						$(this).addClass('selected');

						mymap.setFilter("wdpaAcpSelected", ['==', 'wdpaid', parseInt(selSettings.WDPAID)]);
						mymap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');

						$('html, body').animate({
							scrollTop: $("#biopama_map").offset().top - 100
						}, 1000);
					});
					$(row).hover(
						function() {
							var hoveredPA = parseInt($(this).find('td:first-child').text());
							mymap.setFilter("wdpaAcpHover", ['==', 'wdpaid', parseInt(hoveredPA)]);
							mymap.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');
						}, function() {
							mymap.setLayoutProperty("wdpaAcpHover", 'visibility', 'none');
						}
					);
				},
				rowCallback(row, data, displayNum, displayIndex, dataIndex){
					let wdpaidLink = jQuery(`<a target=”_blank” href="/ct/pa/${data['wdpaid']}">${data['wdpaid']}</a>`);
					let countryLink = jQuery(`<a target=”_blank” href="/ct/country/${data['iso3']}">${data['country_name']}</a>`);
					let paNameLink = jQuery(`<a target=”_blank” href="/ct/pa/${data['wdpaid']}">${data['name']}</a>`);
					jQuery(`td:eq(0)`, row).html(wdpaidLink);
					jQuery(`td:eq(3)`, row).html(countryLink);
					jQuery(`td:eq(5)`, row).html(paNameLink);
				}
			}
		);
		$('#table_assessments_filter').prepend($('.dt-buttons.btn-group'));
	}

	//Drupal.attachBehaviors($("#pame_assessments_table").get(0));
	Drupal.attachBehaviors($(".cql_filters").get(0));

	function pop_filters(data){
		//empty filters
		$.each($('.cql_filters'),function(){
		  $(this).find('option').not(':first').remove();
		});
		var filters={'region':{},'country':{},'year':{},'tool':{}, 'pa_type':{}};

		for (var key in data){
		  filters["region"][ data[key].region ] = true;
		  filters["country"][ data[key].country_name ] = data[key].iso3;
		  filters["year"][ data[key].pame_year ] = true;
		  filters["tool"][ data[key].pame_methodology ] = true;
		  filters["pa_type"][ data[key].pa_type ] = true;
		}

		//Create and append the options
		var selec_tools = document.getElementById("assessment_tools");
		var sortedMethodologies = Object.keys(filters["tool"]).sort();

		for (const key of sortedMethodologies) {
			var option = document.createElement("option");
			option.value = key;
			option.text = key;
			selec_tools.appendChild(option);
		}
		if (sel_filters.pame_methodology != null) $('#assessment_tools').val(sel_filters.pame_methodology);

		var select_year = document.getElementById("assessment_year");
		for (key in filters["year"]) {
			var option = document.createElement("option");
			if (key!="0"){
				option.value = key;
				option.text = key;
			}else{
				option.value = key;
				option.text = "Not Recorded";
			}
			select_year.appendChild(option);
		}
		if (sel_filters.pame_year != null) $('#assessment_year').val(sel_filters.pame_year);

		var select_region = document.getElementById("assessment_region");
		var sortedRegions = Object.keys(filters["region"]).sort();
		for (const key of sortedRegions) {
			var option = document.createElement("option");
			option.value = key;
			option.text = key;
			select_region.appendChild(option);
		}

		if ((sel_filters.region != null) && (sel_filters.region.indexOf("|") <= 0)){
			$('#assessment_region').val(sel_filters.region);
		}

		var select_country = document.getElementById("assessment_country");
		var sortedCountries = Object.keys(filters["country"]).sort();
		for (const key of sortedCountries) {
			var option = document.createElement("option");
			filters["region"]
			option.value = filters["country"][key];
			option.text = key;
			select_country.appendChild(option);
		}
		if (sel_filters.iso3 != null) $('#assessment_country').val(sel_filters.iso3);

		var select_marine = document.getElementById("assessment_pa_type");
		var sortedPAtype = Object.keys(filters["pa_type"]).sort();
		for (const key of sortedPAtype) {
			var option = document.createElement("option");
			option.value = key;
			option.text = key;
			select_marine.appendChild(option);
		}
		if (sel_filters.pa_type != null) $('#assessment_pa_type').val(sel_filters.pa_type);
	}

	function generateRestArgs(){
		var cleanArgs = [];
		for (var propName in sel_filters) {
			if ((sel_filters[propName] != null) || (sel_filters[propName] != undefined)) {
				cleanArgs.push(propName + '=' + sel_filters[propName]);
			}
		}
		return cleanArgs.join(',');
	}

	function getRegions(type = "string", removeACP = true, seperator = "|"){
		var regions = biopamaGlobal.regions
		var regionStr = '';
		var regionArr = [];
		if (removeACP){ //this will remove ACP from the regions
			regions = regions.filter(function( obj ) {
				return obj.id !== 'ACP';
			});
		}

		if (type == "string"){
			for (var region in regions) {
				regionStr += regions[region].name + seperator;
			}
			return regionStr;
		} else {
			for (var region in regions) {
				regionArr.push(regions[region].name);
			}
			return regionArr;
		}


	}

	function setTableData(){

		var selRegion = sel_filters.region;

		if ((selRegion != null) && (selRegion.indexOf("|") <= 0)) {
			$('#assessment_region').val(sel_filters.region);
		} else {
			var regionArg = getRegions();
			sel_filters.region = regionArg;
		}

		var restArguments = generateRestArgs();

		var url = `${biopmaApiBaseUrl}/api_gd_pame_acp/${restArguments}`;

		$.getJSON(url,function(response){
			var assessmentsByWDPA = ['in', 'wdpaid'];
			var currentCountries = [];
			dt_table.clear().draw();

			var regionNames = getRegions();

			response = $.grep(response, function (el, i) {
				if (regionNames.includes(el.region)) {
					var thisWdpa = parseInt(el.wdpaid, 10);
					if(assessmentsByWDPA.indexOf(thisWdpa) === -1) assessmentsByWDPA.push(parseInt(thisWdpa)); //collect all wdpa IDs
					if(currentCountries.indexOf(el.iso3) === -1) currentCountries.push(el.iso3); //collect all countries to zoom to the group
					return true;
				} else {
					return false; // keep the element in the array
				}

			});

			dt_table.rows.add(response).draw();

			mymap.setFilter("wdpaAcpFillHighlighted", assessmentsByWDPA);
			mymap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'visible');

			url = `${biopmaApiBaseUrl}/api_bbox_for_countries_dateline_safe/iso3codes=${currentCountries.join('|')}`;

			$.getJSON(url,function(response){
				mymap.fitBounds(jQuery.parseJSON(response[0].api_bbox_for_countries_dateline_safe));
			});

			mymap.on('click',function(){
				mymap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'none');
			},200);

			// if(firstRun){
			// 	pop_filters(response);
			// 	firstRun=false;
			// }
			pop_filters(response);

		});

	}

	mymap = $.fn.createMap(document.getElementById('biopama_map'));
	$.fn.addMapLayerBiopamaSources(mymap);
	$.fn.addMapLayer(mymap, 'biopamaWDPAPolyJRC');
	mymap.setMinZoom(3);

	mymap.on('load', function () {
		var url_string = new URL( window.location.href );
		for (k in sel_filters){
			if (url_string.searchParams.get(k)){
				sel_filters[k] = url_string.searchParams.get(k);
			}
		}
		createDTables();
		setTableData();
		buildcharts();
		changeTheme();
	});

	mymap.on('click', getFeatureInfo);

	function getFeatureInfo(e){
		var feature = mymap.queryRenderedFeatures(e.point, {
			layers:["wdpaAcpMask"],
		});
		if (feature.length !== 0){
			var url = `${biopmaApiBaseUrl}/api_gd_pame_acp/wdpaid=${feature[0].properties.wdpaid}`;
			$.getJSON(url,function(response){
				if(response.length > 0){
					paPopupContent = '<center class=" text-black"><i class="fas fa-2x fa-paste"></i><p>'+response[0].name+' ('+response[0].wdpaid+')</p>';
					for (var key in response) {
						paPopupContent += '<i>'+response[key].pame_methodology+' ('+response[key].pame_year+')</i><hr>';
					}
					paPopupContent += '</center>';
					new mapboxgl.Popup()
						.setLngLat(e.lngLat)
						.setHTML(paPopupContent)
						.addTo(mymap);
				}
			});
		}
	}

	$('.cql_filters').on('change', function() {
		var currentURL = document.location.href;
		var parameter = $(this).attr('param');
		var value = $(this).val();
		if (value == '%'){
			if (parameter == "region"){
				var regionArg = getRegions();
				sel_filters[parameter] = regionArg;
			} else {
				sel_filters[parameter] = null;
			}
			if(currentURL.includes(parameter)) {
				currentURL = removeURLParameter(currentURL, parameter);
			}
		} else {
			sel_filters[parameter] = value;
			if(currentURL.includes('?')) {
				var currentURL = document.location.href + "&" + parameter + "=" + value;
			}else{
				var currentURL = document.location.href + "?" + parameter + "=" + value;
			}
		}
		history.pushState({}, null, currentURL);
		setTableData();
		buildcharts();
	});

	$('#reset_filters').on('click',function(){
		var currentURL = document.location.href;
		if(currentURL.includes('?')) {
			currentURL = currentURL.split('?')[0];
			history.pushState({}, null, currentURL);
		}
		sel_filters = {
			'region':null,
			'iso3':null,
			'pame_year':null,
			'pame_methodology':null,
			'pa_type':null
		};
		setTableData();
		buildcharts();
	});
	function removeURLParameter(url, parameter) {
		var urlparts= url.split('?');
		if (urlparts.length>=2) {

			var prefix= encodeURIComponent(parameter)+'=';
			var pars= urlparts[1].split(/[&;]/g);

			for (var i= pars.length; i-- > 0;) {
				if (pars[i].includes(parameter)) {
					pars.splice(i, 1);
				}
			}
			url= urlparts[0]+'?'+pars.join('&');
			return url;
		} else {
			return url;
		}
	}

	var countUrl = 'https://api.biopama.org/api/pame/function/api_total_assessments_count';
	$.getJSON( countUrl, function(d) {
	  var bigValue = d[0]['acp_count'];
	  var littleValue = ((Number(d[0]['acp_count'])*100)/(Number(d[0]['acp_count'])+Number(d[0]['non_acp_count']))).toFixed(2)
	  var indicatorTitle = 'Total number of PAME assessment in ACP countries'
	  var chartData = {
		title: indicatorTitle,
		bigNumber: bigValue,
		littleNumber: littleValue,
		littleNumberSuffix: '%',
	  }
	  $.fn.createCountChart('#stats-total-assessments-count-acp', chartData);

	  var bigValue = d[0]['non_acp_count'];
	  var littleValue = ((Number(d[0]['non_acp_count'])*100)/(Number(d[0]['acp_count'])+Number(d[0]['non_acp_count']))).toFixed(2)
	  var indicatorTitle = 'Total number of PAME assessment in non ACP countries'
	  var chartData = {
		title: indicatorTitle,
		bigNumber: bigValue,
		littleNumber: littleValue,
		littleNumberSuffix: '%',
	  }
	  $.fn.createCountChart('#stats-total-assessments-count-non-acp', chartData);
	});

	function buildcharts() {
		var restArguments = generateRestArgs();
		if (restArguments) restArguments = ','+restArguments; //add a comma if there are filters since it will crash the REST if the comma is there without filters
		if (biopamaGlobal.regions.length > 2){
			var url = 'https://api.biopama.org/api/pame/function/api_assessments_count_by/count_type=region'+restArguments;
			$.getJSON( url, function(d) {
				var chartData = [];
				var restData = d.sort((a, b) => parseFloat(a.assessments_count) - parseFloat(b.assessments_count)); //ascending
				$.each(restData, function (i, obj) {
					chartData.push({value: obj.assessments_count, name: obj.group_type});
				});
				var indicatorTitle = 'Assessments by region'
				var chartData = {
					title: indicatorTitle,
					series: [{
						name: indicatorTitle,
						type: 'pie',
						radius: ['30%', '50%'],
						avoidLabelOverlap: true,
						data: chartData
					}]
				}
				$.fn.createNoAxisChart('#stats-assessments-count-by-region', chartData);
			});
		} else {
			$('#stats-assessments-count-by-region').closest(".card").addClass("hidden");
			$('#assessment_region').closest("div").addClass("hidden");
		}

		url = 'https://api.biopama.org/api/pame/function/api_assessments_count_by/count_type=country'+restArguments;
		$.getJSON( url, function(d) {
			var chartXaxisData = [];
			var chartSeriesData = [];
			var restData = d.sort((a, b) => parseFloat(a.assessments_count) - parseFloat(b.assessments_count)); //ascending
			$.each(restData, function (i, obj) {
				chartXaxisData.push(obj.group_type);
				chartSeriesData.push(obj.assessments_count);
			});
			var indicatorTitle = 'Assessments by country'
			var chartData = {
				title: indicatorTitle,
				colors: biopamaGlobal.chart.colors.twoTerrestrial,
				yAxis: {
					name: "Number (#)",
				},
				xAxis: {
					type: 'category',
					data: chartXaxisData,
					title: "Country"
				},
				series: [{
				  data: chartSeriesData,
				  type: 'bar',
				}],
			}
			$.fn.createXYAxisChart('#stats-assessments-count-by-country', chartData);
		});

		url = 'https://api.biopama.org/api/pame/function/api_assessments_count_by/count_type=year'+restArguments;
		$.getJSON( url, function(d) {
			var chartXaxisData = [];
			var chartSeriesData = [];
			var restData = d.sort((a, b) => parseFloat(a.assessments_count) - parseFloat(b.assessments_count)); //ascending
			$.each(restData, function (i, obj) {
				chartXaxisData.push(obj.group_type);
				chartSeriesData.push(obj.assessments_count);
			});
			var indicatorTitle = 'Assessments by year'
			var chartData = {
				title: indicatorTitle,
				colors: biopamaGlobal.chart.colors.twoTerrestrial,
				yAxis: {
					name: "Number (#)",
				},
				xAxis: {
					type: 'category',
					data: chartXaxisData,
					title: "Country"
				},
				series: [{
				  data: chartSeriesData,
				  type: 'bar',
				}],
			}
			$.fn.createXYAxisChart('#stats-assessments-count-by-year', chartData);
		});

		url = 'https://api.biopama.org/api/pame/function/api_assessments_count_by/count_type=methodology'+restArguments;
		$.getJSON( url, function(d) {
			var chartXaxisData = [];
			var chartSeriesData = [];
			var restData = d.sort((a, b) => parseFloat(a.assessments_count) - parseFloat(b.assessments_count)); //ascending
			$.each(restData, function (i, obj) {
				chartXaxisData.push(obj.group_type);
				chartSeriesData.push(obj.assessments_count);
			});
			var indicatorTitle = 'Assessments by methodology'
			var chartData = {
				title: indicatorTitle,
				colors: biopamaGlobal.chart.colors.twoTerrestrial,
				yAxis: {
					name: "Number (#)",
				},
				xAxis: {
					type: 'category',
					data: chartXaxisData,
					title: "Country"
				},
				series: [{
				  data: chartSeriesData,
				  type: 'bar',
				}],
			}
			$.fn.createXYAxisChart('#stats-assessments-count-by-methodology', chartData);
		});

	}

});
