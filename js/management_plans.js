
var sel_filters = {
	'wdpaid': null,
	'region': null,
	'iso3':null,
	'page_year':null,
	'page_methodology':null,
	'pa_type':null
};

const null_filters = {
	'title': null,
	'year' : null,
	'country' : null,
	'region' : null
}

var filters_sel = null_filters;

var table_fields = ['wdpaid','region','country_name','iso3','name','desig','page_year','page_methodology','pa_type'];
var table_labels = {
	'wdpaid':'WDPAID',
	'region':'Region',
	'country_name':'Country',
	'iso3':'ISO-3 Code',
	'name':'Protected Area',
	'page_design':'Designation',
	'page_year':'Year',
	'page_methodology':'Methodology',
	'pa_type':'Type'
};

var dt_table;

var pa_docs = {};

var selSettings = {
    paName: 'default',
	WDPAID: 0,
    countryName: 'trans-ACP',
    regionID: null,
	regionName: null,
	ISO2: null,
	ISO3: null,
	NUM: null,
};

jQuery(document).ready(function($) {
	
	Drupal.attachBehaviors($(".view-gd-page-management-plans").get(0));

	function addwdpas(parameters = ''){
		
		var url = "/rest/page/management_plans?format=json" + parameters;
		
 		$.getJSON(url,function(response){
			var assessmentsByWDPA = ['in', 'WDPAID'];
			var currentCountries = [];

			$.each(response,function(idx,obj){
				var thisWdpa = parseInt(obj.wdpa_id, 10);
				if(assessmentsByWDPA.indexOf(thisWdpa) === -1) assessmentsByWDPA.push(thisWdpa); //collect all wdpa IDs
				if(currentCountries.indexOf(obj.iso3) === -1) currentCountries.push(obj.iso3); //collect all countries to zoom to the group
			});

			map.setFilter("wdpaAcpSelected", assessmentsByWDPA);	
			map.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');	
			url = 'https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_bbox_for_countries_dateline_safe?iso3codes='+currentCountries.toString()+'&format=json&includemetadata=false';
			
			$.getJSON(url,function(response){
				map.fitBounds(jQuery.parseJSON(response.records[0].get_bbox_for_countries_dateline_safe));
			});
		});
	}

    map = $.fn.createMap('page_assessments_map');
	map.setMinZoom(3);

    $.fn.addMapLayerBiopamaSources(map);
	$.fn.addMapLayer(map, 'biopamaWDPAPoly');

    map.on('click', getFeatureInfo);


	map.on('load', function () {
		var url_string = new URL( window.location.href );
		for (k in sel_filters){
			if (url_string.searchParams.get(k)){
				sel_filters[k] = url_string.searchParams.get(k);
			}
		}
		addwdpas();
	});
	function getFeatureInfo(e){
		var feature = map.queryRenderedFeatures(e.point, {
			layers:["wdpaAcpMask"],
		});
		if (feature.length !== 0){
			var url = "/rest/page/management_plans?format=json&wdpaid=" + feature[0].properties.WDPAID;
			$.getJSON(url,function(response){
				paPopupContent = '<center class="available text-black"><i class="fas fa-2x fa-paste"></i><p>'+response[0].title+' ('+response[0].wdpa_id+')</p>';
				for (var key in response.records) {
					paPopupContent += '<i>'+response[0].publication_year+' ('+response[0].field_docuement+')</i><hr>';
				}
				paPopupContent += '</center>';
				new mapboxgl.Popup()
					.setLngLat(e.lngLat)
					.setHTML(paPopupContent)
					.addTo(map);	
			});
		}
	}

	Drupal.behaviors.pagewdpazoom = {
		attach: function (context, settings) {

		$('.page-wdpaid').once('updated-view').on('click',function(){
			selSettings.WDPAID = $(this).html();
			$().mapZoomToPA(map, selSettings.WDPAID);
		})
				
	}};

    function filter_map_onfilters($elem){
		parameters = '';

		$elem.each(function(){
			var filter_name = $(this).attr('name');
			var filter_val = $(this).val();
			if(filter_name){
				filters_sel[filter_name] = filter_val;
			}
		});

		//region, country, year , title
		for (fkey in filters_sel){
			if (filters_sel[fkey] != null){
				parameters += '&' + fkey + '=' + filters_sel[fkey];
			}
		}
		
		addwdpas(parameters);
	}

	
	$('#page_managements_filters select').on('change',function(){
		filter_map_onfilters($('#page_managements_filters select, #page_managements_filters input[type="text"]'));
	})

	$('#page_managements_filters input[type="text"]').on('keyup',function(){
		filter_map_onfilters($('#page_managements_filters select, #page_managements_filters input[type="text"]'));
	})

	$('#edit-reset').attr('type','button');
	$('#edit-reset').on('click',function(){
		$('#page_managements_filters input[type="text"]').val('');
		$('#page_managements_filters select#edit-region').val('All');
		$('#page_managements_filters select#edit-country').val('');
		$('#page_managements_filters select#edit-country').trigger("chosen:updated");
		filters_sel = null_filters;
		$('#page_managements_filters select#edit-region').trigger('change');
	});
});
