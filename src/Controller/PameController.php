<?php

namespace Drupal\biopama_pame\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;

/**
 * Provides route responses for the biopama_pame_assessments module.
 */
class PameController extends ControllerBase
{
  public function PAMEContent()
  {
    $element = array(
      '#theme' => 'biopama_pame_module',
    );
    return $element;
  }

  public function keyConceptsContent()
  {
    $element = array(
      '#theme' => 'pame_keyconcepts_page',
    );
    return $element;
  }

  public function assessmentToolsContent()
  {
    $element = array(
      '#theme' => 'pame_assessmenttools_page',
    );
    return $element;
  }

  public function assessmentsContent()
  {
    $element = array(
      '#theme' => 'pame_assessments_page',
    );
    return $element;
  }

  public function resourcesContent()
  {
    $element = array(
      '#theme' => 'pame_resources_page',
    );
    return $element;
  }

  public function managementPlansContent()
  {
    $element = array(
      '#theme' => 'pame_managementplans_page',
    );
    return $element;
  }

  public function resourcesPameImetContent()
  {
    $element = [
      '#theme' => 'pame_imetresources_page',
    ];
    return $element;
  }

  public function resourcesHandbooksContent()
  {
    $element = [
      '#theme' => 'pame_imethandbooksmanualsresources_page',
    ];
    return $element;
  }

  public function resourcesBrochuresAndFlyersContent()
  {
    $element = [
      '#theme' => 'pame_imetbrochuresflyersresources_page',
    ];
    return $element;
  }

  public function resourcesExamplesContent()
  {
    $element = [
      '#theme' => 'pame_imetexamplesimetusesresources_page',
    ];
    return $element;
  }

  public function resourcesTrainingContent()
  {
    $element = [
      '#theme' => 'pame_imettrainingmaterialsresources_page',
    ];
    return $element;
  }

  public function download()
  {
    $element = [
      '#theme' => 'pame_imetdownload_page',
    ];
    return $element;
  }

  public function knowmore()
  {
    $element = [
      '#theme' => 'pame_imetknowmore_page',
    ];
    return $element;
  }

  public function assessmentsPameImetContent()
  {
    $element = [
      '#theme' => 'pame_imetassessments_page',
    ];
    return $element;
  }
}

